package dev.hacksy.platform_view

import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.EditText
import io.flutter.plugin.platform.PlatformView

internal class KotlinTextPlatformView(context: Context, id:Int, creationParams: Map<String?, Any?>): PlatformView {
    private val editText: EditText = EditText(context)

    override fun getView(): View {
        return editText
    }

    override fun dispose() {
    }
    init{
        editText.textSize = 25f
        editText.setBackgroundColor(Color.rgb(255,255,255))
        editText.setText("Initial Value")
    }

}