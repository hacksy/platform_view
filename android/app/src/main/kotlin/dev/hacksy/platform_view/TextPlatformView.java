package dev.hacksy.platform_view;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Map;

import io.flutter.plugin.platform.PlatformView;

public class TextPlatformView implements PlatformView {
    @NonNull private final TextView textView;

    TextPlatformView(@NonNull Context context, int id, @Nullable Map<String,Object> creationParams){
        textView = new TextView(context);
        textView.setTextSize(50);
        android.util.Log.w("Parameters", creationParams.toString());
        textView.setText("Generado desde Android");
        textView.setBackgroundColor(Color.rgb(255,255,255));
    }
    @NonNull
    @Override
    public View getView() {
        return textView;
    }

    @Override
    public void dispose() {

    }
}
