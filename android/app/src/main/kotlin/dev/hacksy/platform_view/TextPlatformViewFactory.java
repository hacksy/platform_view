package dev.hacksy.platform_view;

import android.content.Context;

import androidx.annotation.NonNull;

import java.util.Map;

import io.flutter.plugin.common.StandardMessageCodec;
import io.flutter.plugin.platform.PlatformView;
import io.flutter.plugin.platform.PlatformViewFactory;

public class TextPlatformViewFactory extends PlatformViewFactory {
 //   @NonNull private final BinaryMessenger binaryMessenger;
  //  @NonNull private final View containerView;

    TextPlatformViewFactory(/*@NonNull BinaryMessenger messenger, @NonNull View containerView*/){
        super(StandardMessageCodec.INSTANCE);
       // this.binaryMessenger = messenger;
       // this.containerView = containerView;
    }
    @Override
    public PlatformView create(@NonNull  Context context, int id, @NonNull  Object args) {
        final Map<String,Object> creationParams = (Map<String,Object>)args;
        return new TextPlatformView(context,id,creationParams);
    }
}
