package dev.hacksy.platform_view

import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine

class MainActivity:FlutterActivity() {
    override fun configureFlutterEngine(flutterEngine: FlutterEngine){
        flutterEngine.platformViewsController.registry.registerViewFactory("platform-view", TextPlatformViewFactory());

        flutterEngine.platformViewsController.registry.registerViewFactory("platform-text", KotlinTextPlatfromViewFactory());
    }
}