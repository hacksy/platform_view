package dev.hacksy.platform_view;

import androidx.annotation.NonNull;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;

public class MainActivity_old extends FlutterActivity {
    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine){
        flutterEngine.getPlatformViewsController().getRegistry()
                .registerViewFactory("platform-view",
                        new TextPlatformViewFactory());
    }
}
