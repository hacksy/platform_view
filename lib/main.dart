import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text("PlatformView"),
        ),
        body: PlatformViewWidget(),
      ),
    );
  }
}
class PlatformViewHybridWidget extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
   return PlatformViewLink(surfaceFactory: (BuildContext context,PlatformViewController controller){
     return AndroidViewSurface(controller: controller,
         hitTestBehavior: PlatformViewHitTestBehavior.opaque,
         gestureRecognizers: const <Factory<OneSequenceGestureRecognizer>>{} );
   }, onCreatePlatformView: (PlatformViewCreationParams params){
     return PlatformViewsService.initSurfaceAndroidView(
       id: params.id,
       viewType: 'platform-view',
       layoutDirection: TextDirection.ltr,
       creationParams: {'c': 'd'},
       creationParamsCodec: const StandardMessageCodec(),
     )..addOnPlatformViewCreatedListener(params.onPlatformViewCreated)
     ..create();
   }, viewType: 'platform-view');
  }
}

class PlatformViewWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 200,
          child: AndroidView(
            viewType: 'platform-text',
            layoutDirection: TextDirection.ltr,
            creationParams: {'c': 'd'},
            creationParamsCodec: const StandardMessageCodec(),
          ),
        ),
        Container(
          height: 200,
          child: AndroidView(
            viewType: 'platform-view',
            layoutDirection: TextDirection.ltr,
            creationParams: {'d': 'd'},
            creationParamsCodec: const StandardMessageCodec(),
          ),
        ),
      ],
    );
  }
}